<?php

/**
 * @file
 * uw_scanner_rule_config.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_scanner_rule_config_default_rules_configuration() {
  $items = array();
  $items['uw_scanner_rule_config_publish_replaced_content'] = entity_import('rules_config', '{ "uw_scanner_rule_config_publish_replaced_content" : {
      "LABEL" : "Publish replaced content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "uw_rules", "workbench_moderation" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_published" : { "node" : [ "node" ] } },
        { "uw_rules_condition_enable_scanner" : [] }
      ],
      "DO" : [
        { "node_publish" : { "node" : [ "node" ] } },
        { "workbench_moderation_set_state_during_save" : { "node" : [ "node" ], "moderation_state" : "published" } }
      ]
    }
  }');
  $items['uw_scanner_rule_config_publish_undo_redo_content'] = entity_import('rules_config', '{ "uw_scanner_rule_config_publish_undo_redo_content" : {
      "LABEL" : "Publish undo-redo content",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "REQUIRES" : [ "uw_rules", "rules", "workbench_moderation" ],
      "ON" : { "uw_rules_event_revision_update" : [] },
      "IF" : [ { "uw_rules_condition_enable_scanner_redo" : [] } ],
      "DO" : [
        { "node_publish" : { "node" : [ "node" ] } },
        { "workbench_moderation_set_state_during_save" : { "node" : [ "node" ], "moderation_state" : "published" } }
      ]
    }
  }');
  return $items;
}
