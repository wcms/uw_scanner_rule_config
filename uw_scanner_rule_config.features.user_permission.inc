<?php

/**
 * @file
 * uw_scanner_rule_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_scanner_rule_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access rules debug'.
  $permissions['access rules debug'] = array(
    'name' => 'access rules debug',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'rules',
  );

  // Exported permission: 'administer rules'.
  $permissions['administer rules'] = array(
    'name' => 'administer rules',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'rules',
  );

  // Exported permission: 'administer scanner settings'.
  $permissions['administer scanner settings'] = array(
    'name' => 'administer scanner settings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'scanner',
  );

  // Exported permission: 'bypass rules access'.
  $permissions['bypass rules access'] = array(
    'name' => 'bypass rules access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'rules',
  );

  // Exported permission: 'perform search and replace'.
  $permissions['perform search and replace'] = array(
    'name' => 'perform search and replace',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'scanner',
  );

  return $permissions;
}
