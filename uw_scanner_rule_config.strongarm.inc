<?php

/**
 * @file
 * uw_scanner_rule_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_scanner_rule_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uwaterloo_custom_listing';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uwaterloo_custom_listing'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_blog';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uw_blog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_ct_person_profile';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uw_ct_person_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_event';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uw_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_image_gallery';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uw_image_gallery'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_news_item';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uw_news_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_service';
  $strongarm->value = 0;
  $export['scanner_body_field_revision_body_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_body_field_revision_body_uw_web_page';
  $strongarm->value = 1;
  $export['scanner_body_field_revision_body_uw_web_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_block_visibility_field_revision_field_block_visibility_uw_promotional_item';
  $strongarm->value = 0;
  $export['scanner_field_block_visibility_field_revision_field_block_visibility_uw_promotional_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_body_no_summary_field_revision_field_body_no_summary_uw_promotional_item';
  $strongarm->value = 0;
  $export['scanner_field_body_no_summary_field_revision_field_body_no_summary_uw_promotional_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_body_no_summary_field_revision_field_body_no_summary_uw_site_footer';
  $strongarm->value = 0;
  $export['scanner_field_body_no_summary_field_revision_field_body_no_summary_uw_site_footer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_caption_field_revision_field_caption_uw_home_page_banner';
  $strongarm->value = 0;
  $export['scanner_field_caption_field_revision_field_caption_uw_home_page_banner'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_additional_info_field_revision_field_contact_additional_info_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_additional_info_field_revision_field_contact_additional_info_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_affiliation_field_revision_field_contact_affiliation_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_affiliation_field_revision_field_contact_affiliation_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_location_field_revision_field_contact_location_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_location_field_revision_field_contact_location_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_phone_field_revision_field_contact_phone_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_phone_field_revision_field_contact_phone_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_reason_field_revision_field_contact_reason_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_reason_field_revision_field_contact_reason_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_title_field_revision_field_contact_title_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_title_field_revision_field_contact_title_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_contact_watiam_id_field_revision_field_contact_watiam_id_contact';
  $strongarm->value = 0;
  $export['scanner_field_contact_watiam_id_field_revision_field_contact_watiam_id_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_event_cost_field_revision_field_event_cost_uw_event';
  $strongarm->value = 0;
  $export['scanner_field_event_cost_field_revision_field_event_cost_uw_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_image_gallery_bottom_field_revision_field_image_gallery_bottom_uw_image_gallery';
  $strongarm->value = 0;
  $export['scanner_field_image_gallery_bottom_field_revision_field_image_gallery_bottom_uw_image_gallery'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_profile_affiliation_field_revision_field_profile_affiliation_uw_ct_person_profile';
  $strongarm->value = 0;
  $export['scanner_field_profile_affiliation_field_revision_field_profile_affiliation_uw_ct_person_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_profile_title_field_revision_field_profile_title_uw_ct_person_profile';
  $strongarm->value = 0;
  $export['scanner_field_profile_title_field_revision_field_profile_title_uw_ct_person_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_project_description_field_revision_field_project_description_uw_project';
  $strongarm->value = 0;
  $export['scanner_field_project_description_field_revision_field_project_description_uw_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_randomization_group_field_revision_field_randomization_group_uw_promotional_item';
  $strongarm->value = 0;
  $export['scanner_field_randomization_group_field_revision_field_randomization_group_uw_promotional_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_service_available_field_revision_field_service_available_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_service_available_field_revision_field_service_available_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_service_cost_new_field_revision_field_service_cost_new_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_service_cost_new_field_revision_field_service_cost_new_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_service_length_field_revision_field_service_length_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_service_length_field_revision_field_service_length_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_service_notice_field_revision_field_service_notice_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_service_notice_field_revision_field_service_notice_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_service_request_field_revision_field_service_request_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_service_request_field_revision_field_service_request_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_service_support_field_revision_field_service_support_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_service_support_field_revision_field_service_support_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_sidebar_content_field_revision_field_sidebar_content_uw_service';
  $strongarm->value = 0;
  $export['scanner_field_sidebar_content_field_revision_field_sidebar_content_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_sidebar_content_field_revision_field_sidebar_content_uw_web_page';
  $strongarm->value = 1;
  $export['scanner_field_sidebar_content_field_revision_field_sidebar_content_uw_web_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_undergrad_award_contact_field_revision_field_undergrad_award_contact_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_field_undergrad_award_contact_field_revision_field_undergrad_award_contact_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_undergrad_award_desc_field_revision_field_undergrad_award_desc_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_field_undergrad_award_desc_field_revision_field_undergrad_award_desc_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_undergrad_award_elig_field_revision_field_undergrad_award_elig_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_field_undergrad_award_elig_field_revision_field_undergrad_award_elig_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_undergrad_award_notes_field_revision_field_undergrad_award_notes_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_field_undergrad_award_notes_field_revision_field_undergrad_award_notes_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_undergrad_award_spec_field_revision_field_undergrad_award_spec_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_field_undergrad_award_spec_field_revision_field_undergrad_award_spec_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_field_undergrad_award_valuedesc_field_revision_field_undergrad_award_valuedesc_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_field_undergrad_award_valuedesc_field_revision_field_undergrad_award_valuedesc_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_mode';
  $strongarm->value = 1;
  $export['scanner_mode'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_published';
  $strongarm->value = 0;
  $export['scanner_published'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_regex';
  $strongarm->value = 0;
  $export['scanner_regex'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_contact';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uwaterloo_custom_listing';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uwaterloo_custom_listing'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_blog';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_blog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_ct_person_profile';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_ct_person_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_event';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_home_page_banner';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_home_page_banner'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_image_gallery';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_image_gallery'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_news_item';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_news_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_project';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_promotional_item';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_promotional_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_service';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_site_footer';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_site_footer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_undergraduate_award';
  $strongarm->value = 0;
  $export['scanner_title_node_revision_uw_undergraduate_award'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_title_node_revision_uw_web_page';
  $strongarm->value = 1;
  $export['scanner_title_node_revision_uw_web_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_vocabulary';
  $strongarm->value = array(
    1 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    2 => 0,
    10 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    7 => 0,
    6 => 0,
    21 => 0,
    25 => 0,
    24 => 0,
    20 => 0,
    23 => 0,
    22 => 0,
    19 => 0,
  );
  $export['scanner_vocabulary'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scanner_wholeword';
  $strongarm->value = 1;
  $export['scanner_wholeword'] = $strongarm;

  return $export;
}
